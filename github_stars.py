import concurrent.futures
import os
from typing import Optional

import toml
from github import Github, UnknownObjectException
from github.Repository import Repository

"""Fetch GH repos in parallel and sort them by number of stars"""


def read_access_token(config_file: str):
    config = toml.load(config_file)
    access_token = config["github"]["access_token"]
    return access_token


def fetch_repo(gh: Github, repo_name: str) -> Optional[Repository]:
    try:
        repo = gh.get_repo(repo_name)
        return repo
    except UnknownObjectException:
        print("Not found:", repo_name)


def fetch_repos(gh: Github, repo_names: list[str]) -> list[Repository]:
    repos = []
    with concurrent.futures.ThreadPoolExecutor() as executor:
        # Submit tasks for fetching repos
        futures = [executor.submit(fetch_repo, gh, repo_name) for repo_name in repo_names]
        # Process completed tasks and store star counts in the dictionary
        for future in concurrent.futures.as_completed(futures):
            repo = future.result()
            if repo is not None:
                repos.append(repo)
    return repos


repo_list = [
    "Abstract-IDE/Abstract",
    "SpaceVim/SpaceVim",
    "CosmicNvim/CosmicNvim",
    "artart222/CodeArt",
    "LazyVim/LazyVim",
    "NTBBloodbath/doom-nvim",
    "crivotz/nv-ide",
    "LunarVim/LunarVim",
    "hackorum/VapourNvim",
    "vi-tality/neovitality",
    "siduck76/NvChad",
    "nvoid-lua/nvoid",
    "cstsunfu/.sea.nvim",
    "shaeinst/roshnivim",
    "AstroNvim/AstroNvim",
    "shaunsingh/nyoom.nvim",
    "CanKolay3499/CNvim",
    "jrychn/moduleVim",
    "askfiy/nvim",
    "imbacraft/dusk.nvim",
    "nvim-lua/kickstart.nvim",
    "cunderw/nvim",
    "otavioschwanck/mood-nvim",
    "ldelossa/nvim-ide",
    "jonathandion/web-dev.nvim",
    "linrongbin16/lin.nvim"
]

secrets = os.path.expanduser("~/.config/scripts/python_secrets.toml")

if __name__ == "__main__":
    access_token = read_access_token(secrets)
    github = Github(access_token)

    repos = fetch_repos(github, repo_list)

    # Sort repositories by star count in descending order
    sorted_repos = sorted(
            repos,
            key=lambda x: x.stargazers_count,
            reverse=True)

    print("Top 10 projects (ordered by stars):")
    for repo in sorted_repos[:10]:
        print(f"{repo.name}: {repo.stargazers_count} stars")
