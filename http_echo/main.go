package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

const listenAddr string = ":8080"

func main() {

	http.HandleFunc("/", EchoHandler)

	log.Println("Starting server at", listenAddr)
	log.Fatal(http.ListenAndServe(listenAddr, nil))
}

func EchoHandler(w http.ResponseWriter, req *http.Request) {
	log.Println("Got request:")
	fmt.Println("")
	// this prints the HTTP request, including the body. This is good enough in most cases
	req.Write(os.Stdout)
	fmt.Println("")
	fmt.Fprintf(w, "ok")
}
