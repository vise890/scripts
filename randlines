#!/usr/bin/env python3
"""Prints random lines from a file."""

import argparse
import random


def lines(path):
    """Returns the lines of a file at `path`, as a generator"""
    with open(path, 'r') as file:
        for line in file.readlines():
            yield line.strip()


def random_elements(elements, n_elements):
    """returns `n_elements` random elements"""
    els = list(elements)
    random.shuffle(els)
    return els[0:n_elements]


def random_lines(path, n_lines):
    """returns `n_lines` random lines from the file at `path`"""
    _lines = lines(path)
    return random_elements(_lines, n_lines)


def print_random_lines(path, n_lines, header_msg=None):
    if header_msg is None:
        header_msg = "Random lines from {}".format(path)
    _lines = "\n".join(random_lines(path, n_lines))
    msg = "{}\n\n{}\n".format(header_msg, _lines)
    print(msg)


def parse_args():
    parser = argparse.ArgumentParser(description=__doc__, )
    parser.add_argument(
        '-n',
        '--n-lines',
        dest='n_lines',
        type=int,
        help='Number of lines to return',
        default=5)
    parser.add_argument(
        '-m', '--message', dest='message', help='message to print')
    parser.add_argument(
        'path', metavar='PATH', help='The path of the file to read')
    parser.set_defaults(
        func=
        lambda args: print_random_lines(args.path, args.n_lines, args.message))
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    args.func(args)
