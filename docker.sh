# shellcheck shell=bash

docker_rm() {
  sudo -v
  local thing="$1"
  shift

  echo "==> removing all docker ${thing}s. commencing"
  docker "$thing" ls --quiet "$@" | xargs --no-run-if-empty -- docker "$thing" rm --force
  echo "~~> removed all docker ${thing}s. done."
}

docker_container_stop_all() {
  # TODO: --filter=running
  echo "==> stopping all docker containers. commencing"
  docker container ls --quiet | xargs --no-run-if-empty -- docker container stop
  echo "~~> stopped all docker containers. done."
}

docker_container_rm_all() {
  docker_container_stop_all
  docker_rm "container" "--all"
}

docker_image_rm_all() {
  docker_rm "image" "--all"
}

docker_network_rm_all() {
  docker network ls --quiet | xargs --no-run-if-empty -- docker network rm
}

docker_volume_rm_all() {
  docker_rm "volume"
  # FIXME: why is this needed?
  echo "==> pruning all docker volumes. commencing"
  docker volume prune --force
  echo "~~> pruned all docker volumes. done."
}

docker_EVERYTHING_rm() {
  docker_container_rm_all
  docker_image_rm_all
  docker_volume_rm_all
  docker_network_rm_all
  docker system prune --all --force --volumes
}
