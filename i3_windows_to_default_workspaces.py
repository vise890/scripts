#!/usr/bin/env python3

"""Assigns windows to their preferred workspaces"""

import i3ipc

# I don't want to set these as the defaults in the i3 config, as I may want to
# move, say, the main IntelliJ window to another workspace, and it is super
# annoying for Ctrl+Shift+F to bring up the search floating window in the
# default IntelliJ workspace rather than the one I'm working on.


# use this cmd to find a window's class:
#   xprop | rg 'CLASS'

window_class_to_workspace = {
    "Emacs": "3",
    "Logseq": "5",
    "firefox": "1",
    "jetbrains-datagrip": "4",
    "jetbrains-idea": "2",
    "teams-for-linux": "5",
}


def get_preferred_workspace_name(window):
    return window_class_to_workspace.get(window.window_class)


def send_windows_to_preferred_workspaces():
    i3 = i3ipc.Connection()
    windows = i3.get_tree().leaves()

    for window in windows:
        workspace_name = get_preferred_workspace_name(window)
        if workspace_name:
            window.command("move to workspace " + workspace_name)


if __name__ == "__main__":
    send_windows_to_preferred_workspaces()
