#!/bin/env python3

import json
import subprocess
import sys
from dataclasses import dataclass
from pathlib import Path

# NOTE: This script reads an AndOTP generator backup file,
# backup file, PGP encrypted, at the path below
#
# https://github.com/andOTP/andOTP


andotp_backup_file = (
    Path.home() / "Documents" / "secrets" / "andOTP" / "otp_accounts.json.gpg.pgp"
)


@dataclass
class OTPEntry:
    type: str
    thumbnail: str
    last_used: str
    used_frequency: str
    tags: str

    label: str
    issuer: str
    secret: str
    algorithm: str
    digits: int
    period: int


def decrypt(file):
    """Decrypt file using GPG."""
    result = subprocess.run(["gpg", "--decrypt", file], capture_output=True, text=True)
    if result.returncode != 0:
        sys.stderr.write(f"Error decrypting fie: {result.stderr}")
        sys.exit(1)
    return result.stdout


def load_otp_db(backup_file):
    entries_raw = json.loads(decrypt(backup_file))
    return [OTPEntry(**e) for e in entries_raw]


def generate_token(entry: OTPEntry):
    """Generate a TOTP token using oathtool."""
    cmd = [
        "oathtool",
        f"--totp={entry.algorithm.lower()}",
        f"--digits={entry.digits}",
        f"--time-step-size={entry.period}",
        "--base32",
        entry.secret,
    ]
    result = subprocess.run(cmd, capture_output=True, text=True)
    if result.returncode != 0:
        sys.stderr.write(result.stderr)
        sys.exit(2)
    return result.stdout.strip()


if __name__ == "__main__":
    otp_db = load_otp_db(andotp_backup_file)

    for entry in otp_db:
        token = generate_token(entry)
        if not (entry.issuer == ""):
            print(f"{token} : {entry.label} : {entry.issuer}")
        else:
            print(f"{token} : {entry.label}")
