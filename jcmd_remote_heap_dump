#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail
IFS=$'\n\t'

# takes a remote GC.heap_dump with jcmd

# It will first prompt you to identify the container on $NODE.  Then, a heap
# dump will be taken and eventually copied on the local machine at
# $HEAP_DUMP_LOCAL_PATH

NODE="$1"

echo "==> identifying container running on node $NODE"
CONTAINER_ID=$(ssh "$NODE" -- \
  docker ps | \
  # exclude header
  tail -n +2 | \
  # interactively select container
  fzf | \
  # squash whitespace
  tr -s ' ' | \
  # get the last field
  rev | cut -d ' ' -f1 | rev
)
echo "==> identifying java PID running in container $CONTAINER_ID"
JAVA_PID=$(ssh "$NODE" -- \
  docker exec "$CONTAINER_ID" jcmd | \
  # chose the process that is not jcmd
  grep -v "sun.tools.jcmd.JCmd" | \
  cut -d ' ' -f1 | \
  xargs
)

HEAP_DUMP_FILENAME="$CONTAINER_ID@${NODE}_$(date -u +"%Y-%m-%dT%H:%M:%SZ").hprof"
HEAP_DUMP_REMOTE_PATH="/tmp/$HEAP_DUMP_FILENAME"
HEAP_DUMP_LOCAL_PATH="./$HEAP_DUMP_FILENAME"

echo "==> taking heap dump of java PID: $JAVA_PID, in container ID: $CONTAINER_ID, on node: $NODE"
ssh "$NODE" -- docker exec "$CONTAINER_ID" rm -f "$HEAP_DUMP_REMOTE_PATH"
ssh "$NODE" -- docker exec "$CONTAINER_ID" jcmd "$JAVA_PID" GC.heap_dump "$HEAP_DUMP_REMOTE_PATH"

echo "==> copying dump onto $NODE (from container $CONTAINER_ID)"
ssh "$NODE" -- docker cp "$CONTAINER_ID:$HEAP_DUMP_REMOTE_PATH" "$HEAP_DUMP_REMOTE_PATH"

echo "==> copying dump to localhost (from node $NODE)"
echo "scp $NODE:$HEAP_DUMP_REMOTE_PATH $HEAP_DUMP_LOCAL_PATH"
scp "$NODE:$HEAP_DUMP_REMOTE_PATH" "$HEAP_DUMP_LOCAL_PATH"
