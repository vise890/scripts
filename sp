#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail
IFS=$'\n\t'

emacs "$NOTES_DIR/__SPARKFILE.org" &
disown
