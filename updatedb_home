#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail
IFS=$'\n\t'

# updatedb, for my home to be used by ./plocate_home_open or other plocates

sudo -v

db_dir="$HOME/.local/lib/plocate"

mkdir -p "$db_dir"

prunepaths=$(cat << PPATHS
$HOME/.1password
$HOME/.android
$HOME/.aqbanking
$HOME/.aws
$HOME/.cabal
$HOME/.cache
$HOME/.cargo
$HOME/.charles
$HOME/.clojure
$HOME/.codestream
$HOME/.config
$HOME/.deps.clj
$HOME/.docker
$HOME/.doom.emacs.d
$HOME/.dotnet
$HOME/.dvdcss
$HOME/.eclipse
$HOME/.emacs.d
$HOME/.embedmongo
$HOME/.fzf
$HOME/.gitlibs
$HOME/.gnupg
$HOME/.gradle
$HOME/.gsutil
$HOME/.hex
$HOME/.icons
$HOME/.ideaLibSources
$HOME/.java
$HOME/.javacpp
$HOME/.jdks
$HOME/.kde4
$HOME/.kodi
$HOME/.lein
$HOME/.local
$HOME/.logseq
$HOME/.lsp
$HOME/.luarocks
$HOME/.m2
$HOME/.mcfly
$HOME/.mitmproxy
$HOME/.mozilla
$HOME/.mplayer
$HOME/.mx.cider
$HOME/.mysql
$HOME/.nbprofiler
$HOME/.nix-defexpr
$HOME/.node-gyp
$HOME/.npm
$HOME/.nuget
$HOME/.nv
$HOME/.nvm
$HOME/.oh-my-zsh
$HOME/.openjfx
$HOME/.parallel
$HOME/.pki
$HOME/.psensor
$HOME/.putty
$HOME/.qomui
$HOME/.rdm
$HOME/.repoconfig
$HOME/.rustup
$HOME/.scm_breeze
$HOME/.screenlayout
$HOME/.siege
$HOME/.solargraph
$HOME/.ssh
$HOME/.steam
$HOME/.step
$HOME/.streamlit
$HOME/.stversions
$HOME/.swt
$HOME/.tabula
$HOME/.templateengine
$HOME/.terraform.d
$HOME/.var
$HOME/.vim
$HOME/.visidata
$HOME/.visualvm
$HOME/.vpython-root
$HOME/.vpython_cipd_cache
$HOME/.vscode
$HOME/.vscode-oss
$HOME/.vsts
$HOME/.w3m
$HOME/.wine
$HOME/.yarn
$HOME/.yjp
$HOME/.zoom
$HOME/Android
$HOME/DataGripProjects
$HOME/Fonts
$HOME/PycharmProjects
$HOME/ScreenRecordings
$HOME/code
PPATHS
)


prunenames=$(cat << PNAMES
.cache
.git
.local
.stversions
node_modules
PNAMES
)

sudo updatedb \
  --output "$HOME/.local/lib/plocate/plocate.db" \
  --database-root "$HOME" \
  --add-prunepaths "$prunepaths" \
  --add-prunenames "$prunenames" # --verbose
