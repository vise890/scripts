#!/bin/zsh

function omz_theme_showcase() {
  for t in $(omz theme list); do
    echo "$t";
    omz theme use "$t";
    print -P "$PS1";
    print -P "$PS1";
    sleep 1.5;
  done
}

alias oh_my_zsh_theme_showcase=omz_theme_showcase
