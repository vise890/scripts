package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"sort"
)

// sucks up all the stdin, then prints matchin followed by non-matching
func main() {
	re := regexp.MustCompile(os.Args[1])

	matching := make([]string, 0)
	non_matching := make([]string, 0)

	scanner := bufio.NewScanner(os.Stdin)

	for scanner.Scan() {
		txt := scanner.Text()
		if txt != "" {
			if re.Match([]byte(txt)) {
				matching = append(matching, txt)
			} else {
				non_matching = append(non_matching, txt)
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	sort.Strings(matching)
	sort.Strings(non_matching)

	for _, l := range matching {
		fmt.Println(l)
	}

	// some whitespace to separate
	fmt.Println()
	fmt.Println()

	for _, l := range non_matching {
		fmt.Println(l)
	}

}
