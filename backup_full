#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail
IFS=$'\n\t'

sudo -v
source "backup.sh"

# rsyncs /boot to $target_partition_boot and / onto $target_partition_root
# assumes root is dm-crypt encrypted

backup_mount

echo "==> $(date) starting boot backup"
sudo rsync --archive \
      --delete \
      --acls \
      --xattrs \
      --owner \
      --group \
      --verbose \
      --progress \
      /boot/ "$backup_boot_mountpoint"
sync --file-system "$backup_boot_mountpoint"
echo "==> $(date) finished boot backup"

echo "==> $(date) starting root backup"
sudo rsync \
      --archive \
      --delete \
      --acls \
      --xattrs \
      --owner \
      --group \
      --verbose \
      --progress \
      --exclude=/dev/* \
      --exclude=/proc/* \
      --exclude=/sys/* \
      --exclude=/tmp/* \
      --exclude=/run/* \
      --exclude=/mnt/* \
      --exclude=/backup/* \
      --exclude=/lost+found/* \
      --exclude=/etc/fstab \
      --exclude="/home/vise890/.cache" \
      --exclude=/nix/* \
      --exclude=/shared/* \
      / /mnt/backup_root
sync --file-system "$backup_root_mountpoint"
echo "==> $(date) finished root backup"

# SEE ALSO:
#   https://weeraman.com/on-linux-backups-and-encrypted-filesystems-8813bf1cad71
