#!/usr/bin/env python3

import argparse
import subprocess

from rich import print
from semantic_version import Version


def parse_args():
    parser = argparse.ArgumentParser(description="Tags with next version")
    parser.add_argument(
        "--git-branch",
        metavar="git_branch",
        default="develop",
        help="the Git branch to tag",
    )
    parser.add_argument(
        "--git-origin",
        metavar="git_origin",
        default="origin",
        help="the Git remote to push tags to",
    )
    parser.add_argument(
        "--git-tag-prefix",
        metavar="git_tag_prefix",
        help="the prefix to use for Git tags",
    )
    parser.add_argument(
        "--version-increment-level",
        metavar="version_increment_level",
        default="build",
        choices=["build", "patch", "minor", "major"],
        help="the level of the version number to increment",
    )
    args = parser.parse_args()
    return args


def ensure_current_branch(expected_current_branch: str):
    current_branch = (
        subprocess.check_output(["git", "rev-parse", "--abbrev-ref", "HEAD"])
        .strip()
        .decode("utf-8")
    )
    if current_branch != expected_current_branch:
        print(f"You are not on the {expected_current_branch} branch, aborting")
        exit(1)


def _version_next_build(version: Version):
    version.build = version.build + 1


def increment_version(prefix: str, increment_level: str, current_version: str):
    current_version_no_prefix = current_version[len(prefix) :]
    version = Version(current_version_no_prefix)
    level_to_method = {
        "build": lambda: _version_next_build(version),
        "patch": version.next_patch,
        "minor": version.next_minor,
        "major": version.next_major,
    }
    next_version = level_to_method[increment_level]()
    return f"{prefix}{next_version}"


def find_previous_tag(git_tag_prefix: str):
    previous_tag = (
        subprocess.check_output(
            f'git describe --match "{git_tag_prefix}*" --abbrev=0 --tags `git rev-list --tags --max-count=1`',
            shell=True,
        )
        .strip()
        .decode("utf-8")
    )
    return previous_tag


if __name__ == "__main__":
    args = parse_args()
    print(f"Args: {args}")

    ensure_current_branch(args.git_branch)

    previous_tag = find_previous_tag(args.git_tag_prefix)
    next_tag = increment_version(
        args.git_tag_prefix, args.version_increment_level, previous_tag
    )
    cmds = [
        ["git", "tag", next_tag],
        ["git", "push", args.git_origin, next_tag],
    ]

    print(f"Previous tag: {previous_tag}")
    print(f"Next tag: {next_tag}")

    print()
    print(f"~~> these commands will be executed:\n{cmds}")
    print()
    answer = input("OK? Type 'yes' to continue...")

    if answer == "yes":
        print("OK, doing it")
        for cmd in cmds:
            subprocess.check_call(cmd)
    else:
        print("OK, aborting")
        exit(1)
