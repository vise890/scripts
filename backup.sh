# shellcheck shell=bash

# https://wiki.archlinux.org/title/Persistent_block_device_naming#by-id_and_by-path
#   ls -l /dev/disk/by-id/
# exports for copy-pastaing
export target_partition_boot="/dev/disk/by-id/nvme-Sabrent_Rocket_4.0_1TB_CD8E070C199E92341031-part1"
export target_partition_root="/dev/disk/by-id/nvme-Sabrent_Rocket_4.0_1TB_CD8E070C199E92341031-part2"

export backup_boot_mountpoint="/mnt/backup_boot"
export backup_root_cryptsetup_name="backup_root"
export backup_root_mountpoint="/mnt/backup_root"
