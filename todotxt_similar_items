#!/usr/bin/env python3

"Read a todo.txt file and print similar lines"

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

import re
import sys

# You may want to set a similarity threshold for considering lines as similar
similarity_threshold = 0.45

# Read the packing lists
with open(sys.argv[1], 'r') as file:
    lines = file.readlines()

# Preprocess lines by removing contexts and projects
def preprocess(line):
    # Using regex to remove tokens that start with @ or +
    return re.sub(r'[@+]\w+', '', line)

processed_lines = [preprocess(line) for line in lines]

# Create a TF-IDF vectorizer
vectorizer = TfidfVectorizer()

# Transform lines into TF-IDF vectors
tfidf_matrix = vectorizer.fit_transform(processed_lines)

# Compare the similarity of the lines using cosine similarity
similarities = cosine_similarity(tfidf_matrix)


# Find and print similar lines
similar_lines_count = 0
for i in range(len(lines)):
    for j in range(i + 1, len(lines)):
        if similarities[i, j] > similarity_threshold:
            similar_lines_count += 1
            print(f"Line {i} is similar to Line {j}:\n{lines[i]}{lines[j]}")

print(f"Found {similar_lines_count} similar lines")
